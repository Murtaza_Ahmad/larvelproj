@extends('layouts.master')
@section('title')
Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Edit Week Planning') }} </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
@foreach($planning as $plan)
  <form action="{{url('admin/UpdateWeekPlanning/'.$plan->id)}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{method_field('PUT') }}
  <div class="modal-body">

    <div class="form-group">
      <label for="planning-title" class="col-form-label">{{ __('Week Planning Title') }}:</label>
      <input type="text" name="planning_title" class="form-control" id="planning_title" placeholder="{{ __('Enter Planning Title') }}" required value="{{$plan->Title}}">
    </div>
    <div class="form-group">
      <label for="recipient-name" class="col-form-label">{{ __('Enter Week #') }}:</label>
      <select name="planning_week" id="planning_week" class="form-control" required>
        <option selected value="{{$plan->Week_Number}}">{{$plan->Week_Number}}</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
      </select>
    </div>

    <div>
      <label for="project_file" style="color:black;">{{ __('Add File') }} :</label><br>
      <input type="file" name="project_file"  id="project-file" placeholder="Enter File Here"  required value="{{$plan->pdf_file}}">
    </div>

  </div>

  <div class="modal-footer">
  <a href="{{ url('admin/projects') }}" type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</a>
  <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
  </div>
  </form>
  @endforeach
</div>

</div>
</div>
</div>
</div>
@endsection
