<?php $__env->startSection('title'); ?>
Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Creating New Project')); ?> </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
  <form action="<?php echo e(url('/admin/AddProjects/submit')); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>


  <div class="modal-body">

    <div class="form-group">
      <label for="project-title" class="col-form-label" style="color:black;"><?php echo e(__('Project Title')); ?></label>
      <input type="text" name="projecttitle" class="form-control" id="projecttitle" placeholder="<?php echo e(__('Enter Project Title Here')); ?>"  required>
    </div>
    <div class="form-group">
      <label for="project-description" class="col-form-label" style="color:black;"><?php echo e(__('Project Description')); ?></label>
      <textarea class="form-control" rows="7" placeholder="<?php echo e(__('Enter Description Here')); ?>" name="project_description">
      </textarea>
    </div>
    <div>
      <label for="project_file" style="color:black;"><?php echo e(__('Add File')); ?>:</label><br>
      <input type="file" name="project_file"  id="project-file" placeholder="Enter File Here"  required>
    </div>

  </div>

  <div class="modal-footer">
  <a href="<?php echo e(url('admin/projects')); ?>" type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></a>
  <button type="submit" class="btn btn-primary"><?php echo e(__('Add')); ?></button>
  </div>
  </form>
</div>

</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/AddProjects.blade.php ENDPATH**/ ?>