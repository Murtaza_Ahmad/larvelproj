<?php $__env->startSection('content'); ?>

    <section class="section">

    <!-- Code Here for the home page -->
    <br>

    <div class="col-12 col-sm-12 col-lg-8">
      <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="card card-primary">

        <div class="card-body">
          <p style="text-align:right">
            <?php echo e(__('Created At')); ?> :  <?php echo e($message->created_at); ?>

          </p>
        <h4 style="color:black;"><?php echo e($message->News_Title); ?></h4>
          <p><?php echo e($message->News_Messages); ?></p>
        </div>
      </div><br>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</section>
<br>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/News-details.blade.php ENDPATH**/ ?>