<?php

namespace App\Http\Controllers;
use App\Planning;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PlanningController extends Controller
{
  //For Adding Plannings into database
    public function add_planning(Request $request){
      $planning = new Planning;
      $planning->Title = $request->input('planning_title');
      $planning->Week_Number = $request->input('planning_week');

      //If file is present, or files
      if($request->hasFile('project_file'))
      {

        $allowedfileExtension= 'pdf';    //just pdf can be uploaded
        $file = $request->file('project_file');
        $name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if ($allowedfileExtension == $extension) {
        //working
        $destinationPath = public_path('Week_Plannings');

        $file->move($destinationPath, $name);
        $planning->pdf_file = $name;

        $planning->save();
        }
        else {  //If file is not pdf
          echo "<h3>The uploaded file is not pdf. Upload was unsuccessful!</h3>";
            return redirect('/admin/WeekPlanning')->with('status','Upload Unsuccessful');
        }
      }
      else {  //If there's no file
        return $request;
      }
      return redirect('/admin/WeekPlanning')->with('status','Project Created Successfully!');

    }

    public function fetch_plannings(){
      $plannings = Planning::all();
      return view('admin.week-planning')->with('plannings',$plannings);
    }

    public function fetch_user_plannings(){
      $plannings = Planning::orderBy('id', 'desc')->get();
      return view('All-Plannings')->with('plannings',$plannings);
    }

    public function delete_planning(Request $request){
          $planning_id = $request->route('id');
          if($planning_id){

            if (DB::table('plannings')->where('id', '=', $planning_id)->delete()) {
              $data = "Week planning deleted successfully!";
            }
            else {
              $data = "Wooops! Operation Unsuccessful!";
            }

          }
          else {
            $data = "Wooops! Operation Unsuccessful!";
          }
  return redirect('/admin/WeekPlanning')->with('status',$data);
    }

    public function edit_plannings($id){
      $planning = DB::table('plannings')->where('id', '=', $id)->get();
        return view('admin.EditWeekPlanning')->with('planning',$planning);
    }

    public function update_plannings(Request $request, $id){

      $planning = Planning::findOrFail($id);
      $planning->Title = $request->input('planning_title');
      $planning->Week_Number = $request->input('planning_week');

      //If file is present, or files
      if($request->hasFile('project_file'))
      {

        $allowedfileExtension = 'pdf';    //just pdf can be uploaded
        $file = $request->file('project_file');
        $name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        if ($allowedfileExtension == $extension) {
        //working
        $destinationPath = public_path('Week_Plannings');

        $file->move($destinationPath, $name);
        $planning->pdf_file = $name;

        if($planning->update())
        $data = "Week Planning updated Successfully!";
        else
        $data = "Woops! Update Unsuccessful!";
        }
        else {  //If file is not pdf
          $data = "Update Unsuccessful! File is not pdf";
            return redirect('/admin/WeekPlanning')->with('status',$data);
        }
      }
      else {  //If there's no file
        return $request;
      }
      return redirect('/admin/WeekPlanning')->with('status',$data);
}
}
