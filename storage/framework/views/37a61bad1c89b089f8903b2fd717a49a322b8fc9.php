<?php $__env->startSection('content'); ?>

    <section class="section">

    <!-- Code Here for the home page -->
    <br><br><br>

    <div class="col-12 col-sm-12 col-lg-12">
      <h2 style="color:black;"><?php echo e(__('News')); ?></h2>
      <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="card card-primary">


          <div class="card-header-action">

          </div>
        <div class="card-body">
          <p style="text-align:right">
            <?php echo e(__('Created At')); ?> :
            <?php echo e($message->created_at); ?>

        </p>
        <h4><?php echo e($message->News_Title); ?></h4>
          <p><?php echo e($message->News_Messages); ?></p>
        </div>
        <div class="text-right mt-sm-0 mt-3">
            <a href="<?php echo e(url('/All-News/'.$message->id)); ?>" class="btn btn-primary" style="font-size:15px;margin-right:12px;margin-bottom:12px;"><?php echo e(__('Read Further')); ?> :<i class="dripicons-arrow-right"></i></a>
        </div>
      </div><br>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</section>
<br>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/All-News.blade.php ENDPATH**/ ?>