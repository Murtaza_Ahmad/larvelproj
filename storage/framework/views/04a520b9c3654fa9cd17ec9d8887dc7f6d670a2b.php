<?php $__env->startSection('title'); ?>
Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Edit Project Access')); ?> </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
<?php $__currentLoopData = $project_assigned; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <form action="<?php echo e(url('admin/UpdateProjectAccess/'.$row->id)); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

    <?php echo e(method_field('PUT')); ?>

    <div class="modal-body">
      <div class="form-group">
        <label for="project_name" class="col-form-label"><?php echo e(__('Choose Project')); ?></label>
        <select name="project_name" id="project_name" class="form-control" required>
          <option selected value="<?php echo e($row->Project_ID); ?>"><?php echo e($row->Project_Name); ?></option>
          <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <option value="<?php echo e($proj->Project_Number); ?>"><?php echo e($proj->Project_Title); ?></option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
      </div>

      <div class="form-group">
        <label for="user-name" class="col-form-label"><?php echo e(__('Assign To')); ?></label>
        <select name="user_name" id="user_name" class="form-control" required>
          <option selected value="<?php echo e($row->User_ID); ?>"><?php echo e($row->User_Name); ?></option>
          <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <option value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?></option>

          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
      </div>

  </div>

  <div class="modal-footer">
  <a href="<?php echo e(url('admin/ProjectAccess')); ?>" type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></a>
  <button type="submit" class="btn btn-primary"><?php echo e(__('Update')); ?></button>
  </div>
  </form>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/EditProjectAccess.blade.php ENDPATH**/ ?>