<?php

namespace App\Http\Controllers;
use App\Message;
use DB;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function add_messages(Request $request){
      $news = new Message;
      $news->News_Title = $request->input('newstitle');
      $news->News_Messages = $request->input('news_message');
      $news->Text_Messages = $request->input('text_message');
      $news->save();
      return redirect('/admin/NewsMessages')->with('status','News Created Successfully!');
    }

    public function fetch_news(){
    $messages = Message::all();
      return view('admin.news-messages')->with('messages',$messages);
    }

    //Fetch All News Message for user home
    public function fetch_user_news(){
      $news = Message::orderBy('id', 'desc')->get();
      return view('All-News')->with('news',$news);
    }

    public function fetch_user_news_details(Request $request,$x){
      $id = $request->route('id');    //Storing ID from route
      $data = DB::table('messages')->where('id','=', $id)->get();
      return view('News-details',compact('data'));
    }
    public function delete_message(Request $request){
      $message_id = $request->route('id');
    if($message_id){

      if (DB::table('messages')->where('id', '=', $message_id)->delete()) {
        $data = "News Message deleted successfully!";
      }
      else {
        $data = "Wooops! Project was not deleted!";
      }

    }
    else {
      $data = "Error Occured in deletion!";
    }


  return redirect('/admin/NewsMessages')->with('status',$data);
    //return view()->with('');
}

  public function edit_news($id){
    $message = DB::table('messages')->where('id', '=', $id)->get();
    return view('admin.EditMessages')->with('message',$message);
}

  public function update_news(Request $request, $id){

    $news = Message::findOrFail($id);
    $news->News_Title = $request->input('newstitle');
    $news->News_Messages = $request->input('news_message');
    $news->Text_Messages = $request->input('text_message');
    if ($news->update()) {
      $data = "Data updated successfully!";
    }
    else {
      $data = "Error in updation";
    }


    return redirect('/admin/NewsMessages')->with('status',$data);

  }

}
