@extends('layouts.master')
@section('title')
Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Edit Project') }} </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
  @foreach($proj_id as $proj)
  <form action="{{url('/admin/UpdateProjects/'.$proj->Project_Number)}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{method_field('PUT') }}
    <div class="modal-body">
    <div class="form-group">
      <label for="project-title" class="col-form-label" style="color:black;">{{ __('Project Title') }}</label>
      <input type="text" name="projecttitle" class="form-control" id="projecttitle"  required value="{{ $proj->Project_Title }}">
    </div>
    <div class="form-group">
      <label for="project-description" class="col-form-label" style="color:black;">{{ __('Project Description') }}</label>
      <textarea class="form-control" rows="7" name="project_description" >
        {{ $proj->Description }}
      </textarea>
    </div>
    <div>
      <label for="project_file" style="color:black;">{{ __('Add File') }}:</label><br>
      <input type="file" name="project_file"  id="project-file"  required value="{{ $proj->pdf_file }}">
    </div>

  </div>

  <div class="modal-footer">
  <a href="{{ url('admin/projects') }}" type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</a>
  <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
  </div>
  </form>
  @endforeach
</div>

</div>
</div>
</div>
</div>
@endsection
