@extends('layouts.master')
@section('title')
Users List | Planner
@endsection

@section('content')
<!-- Modal Box for testing -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="width:100%;height:100%;">
    <!--  <div class="modal-header">
-->
<div class="text-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
</div>
  <!--    </div>
-->

      <div class="modal-body">
<!--
<div id="hello" style="width:100%;height:100%;">
-->
  <div>
    <button class="btn btn-primary btn-sm" id="prev">{{ __('Previous') }}</button>
    <button id="next" class="btn btn-info btn-sm">{{ __('Next') }}</button>
    &nbsp; &nbsp;
    <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
  </div>

  <canvas id="the-canvas"></canvas>
<!--</div> -->
      </div>
<!--
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
-->
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Projects List') }} </h4>
      </div>
      @if(session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
        @endif
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class= "text-primary">
              <th>
                {{ __('Project Number') }}
              </th>
              <th>
                {{ __('Title') }}
              </th>
              <th>
                {{ __('Description') }}
              </th>
              <th>
                {{ __('Files') }}
              </th>
              <th>
                {{ __('View') }}
              </th>
              <th>
              {{ __('Edit') }}
              </th>
              <th>
                {{ __('Delete') }}
              </th>
            </thead>
            <tbody>

@foreach($projects as $proj)
              <tr>
                <td> {{$proj->Project_Number}} </td>
                <td> {{ $proj->Project_Title }} </td>
                <td style="padding-right:5px;padding-left:5px;">{{ $proj->Description }}</td>
                @php
                $x = '/portal/public/documents/'.$proj->pdf_file;
                @endphp
                <td><a href="@php echo $x @endphp"> {{ $proj->pdf_file }}</a></td>
                  <td>
                  <button class="btn btn-info btn-sm" onclick="display_pdf('{{$proj->pdf_file}}')" data-toggle="modal"
                    data-target=".bd-example-modal-lg">{{ __('View') }}</button>
                  </td>
                  <td>
                    <a href="{{url('admin/AddProjects/'.$proj->Project_Number)}}"  class="btn btn-warning btn-sm"> {{ __('Edit') }} </a>
                  </td>
                  <td>
                    <a href="{{url('admin/DeleteProject/'.$proj->Project_Number)}}" onclick="return confirm('Are you sure you want to delete it?')" class="btn btn-danger btn-sm"> <i class="fa fa-trash"></i> {{ __('Delete') }} </a>
                  <!--
                  <button class="btn btn-info" data-toggle="modal"
                    data-target=".bd-example-modal-lg"><i class="fa fa-plus"></i>  View</button>
                  -->

                </td>
              </tr>
@endforeach

            </tbody>
          </table>

        </div>
      </div>

    </div>
    <div class="text-center">
      <a href="{{url('/admin/AddProjects ')}}" class="btn btn-info"><i class="fa fa-plus"></i> {{ __('Add New Projects') }}</a>


</div>
       <script type="text/javascript">
function display_pdf(x){
      // var url = '/Folder/public/documents/test2.pdf';
         // If absolute URL from the remote server is provided, configure the CORS
         // header on that server.
        // var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf';

          var url = '/portal/public/documents/'+x;

         // Loaded via <script> tag, create shortcut to access PDF.js exports.
         var pdfjsLib = window['pdfjs-dist/build/pdf'];

         // The workerSrc property shall be specified.
         pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

         var pdfDoc = null,
             pageNum = 1,
             pageRendering = false,
             pageNumPending = null,
             scale = 0.55,
             canvas = document.getElementById('the-canvas'),
             ctx = canvas.getContext('2d');

         /**
          * Get page info from document, resize canvas accordingly, and render page.
          * @param num Page number.
          */
         function renderPage(num) {
           pageRendering = true;
           // Using promise to fetch the page
           pdfDoc.getPage(num).then(function(page) {
             var viewport = page.getViewport({scale: scale});
             canvas.height = viewport.height;
             canvas.width = viewport.width;

             // Render PDF page into canvas context
             var renderContext = {
               canvasContext: ctx,
               viewport: viewport
             };
             var renderTask = page.render(renderContext);

             // Wait for rendering to finish
             renderTask.promise.then(function() {
               pageRendering = false;
               if (pageNumPending !== null) {
                 // New page rendering is pending
                 renderPage(pageNumPending);
                 pageNumPending = null;
               }
             });
           });

           // Update page counters
           document.getElementById('page_num').textContent = num;
         }

         /**
          * If another page rendering in progress, waits until the rendering is
          * finised. Otherwise, executes rendering immediately.
          */
         function queueRenderPage(num) {
           if (pageRendering) {
             pageNumPending = num;
           } else {
             renderPage(num);
           }
         }

         /**
          * Displays previous page.
          */
         function onPrevPage() {
           if (pageNum <= 1) {
             return;
           }
           pageNum--;
           queueRenderPage(pageNum);
         }
         document.getElementById('prev').addEventListener('click', onPrevPage);

         /**
          * Displays next page.
          */
         function onNextPage() {
           if (pageNum >= pdfDoc.numPages) {
             return;
           }
           pageNum++;
           queueRenderPage(pageNum);
         }
         document.getElementById('next').addEventListener('click', onNextPage);

         /**
          * Asynchronously downloads PDF.
          */
         pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
           pdfDoc = pdfDoc_;
           document.getElementById('page_count').textContent = pdfDoc.numPages;

           // Initial/first page rendering
           renderPage(pageNum);
         });

  }

//document.getElementById('hello').innerHTML = "<embed src='/portal/public/documents/"+x+"' frameborder='0' width='100%' height='100%'>";
//document.getElementById('hello').innerHTML = document.getElementById('the-canvas').innerHTML;
//}
</script>
@endsection
