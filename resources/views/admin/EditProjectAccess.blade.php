@extends('layouts.master')
@section('title')
Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Edit Project Access') }} </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
@foreach($project_assigned as $row)
  <form action="{{url('admin/UpdateProjectAccess/'.$row->id)}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{method_field('PUT') }}
    <div class="modal-body">
      <div class="form-group">
        <label for="project_name" class="col-form-label">{{ __('Choose Project') }}</label>
        <select name="project_name" id="project_name" class="form-control" required>
          <option selected value="{{$row->Project_ID}}">{{$row->Project_Name}}</option>
          @foreach($projects as $proj)
          <option value="{{$proj->Project_Number}}">{{$proj->Project_Title}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group">
        <label for="user-name" class="col-form-label">{{ __('Assign To') }}</label>
        <select name="user_name" id="user_name" class="form-control" required>
          <option selected value="{{$row->User_ID}}">{{$row->User_Name}}</option>
          @foreach($users as $user)
          <option value="{{$user->id}}">{{$user->name}}</option>

          @endforeach
        </select>
      </div>

  </div>

  <div class="modal-footer">
  <a href="{{ url('admin/ProjectAccess') }}" type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</a>
  <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
  </div>
  </form>
@endforeach
</div>

</div>
</div>
</div>
</div>
@endsection
