@extends('layouts.master')
@section('title')
Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Creating News Message') }} </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
  <form action="{{url('/admin/AddNewsMessage/submit')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

  <div class="modal-body">

    <div class="form-group">
      <label for="news-title" class="col-form-label" style="color:black;">{{ __('News Title') }}:</label>
      <input type="text" name="newstitle" class="form-control" id="newstitle" placeholder="{{ __('Enter News Title Here') }}"  required>
    </div>
    <div class="form-group">
      <label for="news-description" class="col-form-label" style="color:black;">{{ __('News Message') }}</label>
      <textarea class="form-control" rows="15" maxlength="1000" placeholder="{{ __('Enter News Message Here') }}" name="news_message" required>
      </textarea>
    </div>
  <!--  <div class="form-group">
      <label for="text-message" class="col-form-label" style="color:black;">{{ __('Text Message') }}</label>
      <textarea class="form-control" rows="7" placeholder="{{ __('Enter Text Message Here') }}" name="text_message">
      </textarea>
    </div>
  -->
  </div>

  <div class="modal-footer">
  <a href="{{ url('admin/NewsMessages') }}" type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</a>
  <button type="submit" class="btn btn-primary">{{ __('Add') }}</button>
  </div>
  </form>
</div>

</div>
</div>
</div>
</div>
@endsection
