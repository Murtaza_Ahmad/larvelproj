<?php $__env->startSection('title'); ?>
Projects Access | Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Modal Starts Here-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?php echo e(url('/admin/ProjectAccess/submit')); ?>" method="post" enctype="multipart/form-data">
          <?php echo e(csrf_field()); ?>

      <div class="modal-body">

          <div class="form-group">
            <label for="project_name" class="col-form-label"><?php echo e(__('Choose Project')); ?></label>
            <select name="project_name" id="project_name" class="form-control" required>
              <option selected value=""><?php echo e(__('Select Project')); ?></option>
              <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($proj->Project_Number); ?>"><?php echo e($proj->Project_Title); ?></option>

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>

          <div class="form-group">
            <label for="user-name" class="col-form-label"><?php echo e(__('Assign To')); ?></label>
            <select name="user_name" id="user_name" class="form-control" required>
              <option selected value=""><?php echo e(__('Select User')); ?></option>
              <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($user->id); ?>"><?php echo e($user->name); ?></option>

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo e(__('Save')); ?></button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal Ends Here -->
<div class="row">
  <div class="col-md-12">

    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Manage Project Access')); ?> </h4>
      </div>
      <?php if(session('status')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo e(session('status')); ?>

        </div>
          <?php endif; ?>
      <div class="card-body">
        <div class="table-responsive">

          <table class="table">

<tbody style="text-align:center">
  <tr class="bg-dark text-light">
  <th>
  <?php echo e(__('Project')); ?>

  </th>
  <th>
    <?php echo e(__('Assigned To')); ?>

  </th>
  <th>
    <?php echo e(__('Edit')); ?>/<?php echo e(__('Remove Access')); ?>

  </th>

</tr>
  <?php $__currentLoopData = $access; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <tr style="text-align:center">
    <td><?php echo e($row->Project_Name); ?></td><td><?php echo e($row->User_Name); ?></td>
    <td>
      <a href="<?php echo e(url('admin/ProjectAccess/'.$row->id)); ?>" class="btn btn-warning" style="margin-right:5px;"><?php echo e(__('Edit')); ?></a>
                        <a href="<?php echo e(url('admin/DeleteProjectAccess/'.$row->id)); ?>" onclick="return confirm('Are you sure you want to remove access?')" class="btn btn-danger"> <i class="fa fa-trash"></i> <?php echo e(__('Delete')); ?> </a>
                      </td>
  </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
          </table>
<br>
<!--
<select name="project" style="font-size:18px;padding:6px;">
  <option value="">Select Project</option>
  <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<option value=""><?php echo e($proj->Project_Title); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
<select name="user" style="font-size:18px;padding:6px;">
    <option value="">Select User</option>
  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <option value=""><?php echo e($user->name); ?></option>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
-->
<div class="text-center">
<button class="btn btn-info" data-toggle="modal"
  data-target="#exampleModal"><i class="fa fa-plus"></i> <?php echo e(__('Assign New Project')); ?></button>
</div>
        </div>
      </div>


    </div>

  </div>

</div>
<script>
function update_access(project_id, user_id){
alert("ProjectID is : "+ project_id + " & User ID is : "+ user_id);
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/project-access.blade.php ENDPATH**/ ?>