<?php $__env->startSection('title'); ?>
Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Edit News Messages')); ?> </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
  <?php $__currentLoopData = $message; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <form action="<?php echo e(url('/admin/UpdateNewsMessages/'.$message->id)); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

<?php echo e(method_field('PUT')); ?>

<div class="modal-body">

  <div class="form-group">
    <label for="news-title" class="col-form-label" style="color:black;"><?php echo e(__('News Title')); ?>:</label>
    <input type="text" name="newstitle" class="form-control" id="newstitle" placeholder="<?php echo e(__('Enter News Title Here')); ?>"  required value="<?php echo e($message->News_Title); ?>">
  </div>
  <div class="form-group">
    <label for="news-description" class="col-form-label" style="color:black;"><?php echo e(__('News Message')); ?></label>
    <textarea class="form-control" rows="11" placeholder="<?php echo e(__('Enter News Message Here')); ?>" name="news_message" required value="">
      <?php echo e($message->News_Messages); ?>

    </textarea>
  </div>
  <div class="form-group">
    <label for="text-message" class="col-form-label" style="color:black;"><?php echo e(__('Text Message')); ?></label>
    <textarea class="form-control" rows="7" placeholder="<?php echo e(__('Enter Text Message Here')); ?>" name="text_message" value="">
      <?php echo e($message->Text_Messages); ?>

    </textarea>
  </div>
</div>

  <div class="modal-footer">
  <a href="<?php echo e(url('admin/NewsMessages')); ?>" type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></a>
  <button type="submit" class="btn btn-primary"><?php echo e(__('Update')); ?></button>
  </div>
  </form>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/EditMessages.blade.php ENDPATH**/ ?>