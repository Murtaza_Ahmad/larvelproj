<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  //  protected $redirectTo;
protected function redirectTo(){
  if (Auth::user()->user_type == 'admin') {
    //return 'admin.dashboard';
    return redirect('/admin/dashboard');
  }
  else {
    //return 'home';
    return redirect('/');
  }

}
/*
protected function authenticated(Request $request, $user)
    {
        if (Auth::user()->user_type == 'admin') {
            return redirect('/admin');
        }

        return redirect('/');
    }
  */
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(!file_exists(storage_path()."/installed")){
            header('location:install');
            die;
        }
        $this->middleware('guest')->except('logout');
    }
}
