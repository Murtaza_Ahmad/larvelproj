<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsAssignedtoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_assignedto_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('Project_ID');
            $table->string('Project_Name');
            $table->integer('User_ID');
            $table->string('User_Name');
            $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_assignedto_users');
    }
}
