@extends('layouts.master')
@section('title')
Projects Access | Planner
@endsection

@section('content')
<!-- Modal Starts Here-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="{{url('/admin/ProjectAccess/submit')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
      <div class="modal-body">

          <div class="form-group">
            <label for="project_name" class="col-form-label">{{ __('Choose Project') }}</label>
            <select name="project_name" id="project_name" class="form-control" required>
              <option selected value="">{{ __('Select Project') }}</option>
              @foreach($projects as $proj)
              <option value="{{$proj->Project_Number}}">{{$proj->Project_Title}}</option>

              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="user-name" class="col-form-label">{{ __('Assign To') }}</label>
            <select name="user_name" id="user_name" class="form-control" required>
              <option selected value="">{{ __('Select User') }}</option>
              @foreach($users as $user)
              <option value="{{$user->id}}">{{$user->name}}</option>

              @endforeach
            </select>
          </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal Ends Here -->
<div class="row">
  <div class="col-md-12">

    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Manage Project Access') }} </h4>
      </div>
      @if(session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
          @endif
      <div class="card-body">
        <div class="table-responsive">

          <table class="table">

<tbody style="text-align:center">
  <tr class="bg-dark text-light">
  <th>
  {{ __('Project') }}
  </th>
  <th>
    {{ __('Assigned To') }}
  </th>
  <th>
    {{ __('Edit') }}/{{ __('Remove Access') }}
  </th>

</tr>
  @foreach($access as $row)
  <tr style="text-align:center">
    <td>{{$row->Project_Name}}</td><td>{{$row->User_Name}}</td>
    <td>
      <a href="{{url('admin/ProjectAccess/'.$row->id)}}" class="btn btn-warning" style="margin-right:5px;">{{ __('Edit') }}</a>
                        <a href="{{url('admin/DeleteProjectAccess/'.$row->id)}}" onclick="return confirm('Are you sure you want to remove access?')" class="btn btn-danger"> <i class="fa fa-trash"></i> {{ __('Delete') }} </a>
                      </td>
  </tr>
  @endforeach
</tbody>
          </table>
<br>
<!--
<select name="project" style="font-size:18px;padding:6px;">
  <option value="">Select Project</option>
  @foreach($projects as $proj)
<option value="">{{$proj->Project_Title}}</option>
  @endforeach()
</select>
<select name="user" style="font-size:18px;padding:6px;">
    <option value="">Select User</option>
  @foreach($users as $user)
  <option value="">{{$user->name}}</option>
  @endforeach()
</select>
-->
<div class="text-center">
<button class="btn btn-info" data-toggle="modal"
  data-target="#exampleModal"><i class="fa fa-plus"></i> {{ __('Assign New Project') }}</button>
</div>
        </div>
      </div>


    </div>

  </div>

</div>
<script>
function update_access(project_id, user_id){
alert("ProjectID is : "+ project_id + " & User ID is : "+ user_id);
}
</script>
@endsection
