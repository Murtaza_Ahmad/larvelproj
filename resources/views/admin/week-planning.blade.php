@extends('layouts.master')
@section('title')
Week Planning | Planner
@endsection

@section('content')



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __('Add New Week Plannings') }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="{{url('/admin/AddPlanning/submit')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
      <div class="modal-body">

          <div class="form-group">
            <label for="planning-title" class="col-form-label">{{ __('Week Planning Title') }}:</label>
            <input type="text" name="planning_title" class="form-control" id="planning_title" placeholder="{{ __('Enter Planning Title') }}" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">{{ __('Enter Week #') }}:</label>
            <select name="planning_week" id="planning_week" class="form-control" required>
              <option selected>{{ __('Choose Week') }}</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
          </div>

          <div>
            <label for="project_file" style="color:black;">{{ __('Add File') }} :</label><br>
            <input type="file" name="project_file"  id="project-file" placeholder="Enter File Here"  required>
          </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
        <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--
Next modal
 -->
 <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
     <div class="modal-content" style="width:100%;height:100%;">
       <div class="modal-header">

         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>


       <div class="modal-body" >
 <div id="hello" style="width:100%;height:100%;">

 </div>
       </div>

       <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

       </div>

     </div>
   </div>
 </div>


<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Week Planning') }} </h4>
      </div>
      @if(session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
          @endif
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class= "text-primary" >
              <h2>{{ __('Manage Week Planning') }}</h2>
            </thead>
            <tbody style="text-align:center">
              <th>
                {{ __('Planning Title') }}
              </th>
              <th>
                {{ __('Week #') }}
              </th>
              <th>
                {{ __('Document') }}
              </th>
              <th>
                {{ __('Edit') }}/{{ __('Remove Planning') }}
              </th>

              @foreach($plannings as $row)
              <tr style="text-align:center">
                <td> {{$row->Title }} </td>
                <td> {{ $row->Week_Number }} </td>

                @php
                $x = '/portal/public/Week_Plannings/'.$row->pdf_file;
                @endphp
                <td><a href="@php echo $x @endphp"> {{ $row->pdf_file }}</a></td>
                <td>
                  <a href="{{url('admin/WeekPlanning/'.$row->id)}}" class="btn btn-warning" style="margin-right:5px;">  {{ __('Edit') }} </a>
                <a href="{{url('admin/DeleteWeekPlanning/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete this planning?')" class="btn btn-danger"> <i class="fa fa-trash"></i> {{ __('Delete') }} </a>
                </td>
              <!--<td>  <button class="btn btn-info" onclick="display_pdf('{{$row->pdf_file}}')" data-toggle="modal"
                  data-target=".bd-example-modal-lg">View Document</button></td>
-->
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="text-center">
      <button class="btn btn-info" data-toggle="modal"
        data-target="#exampleModal"><i class="fa fa-plus"></i>  {{ __('Add New Week Plannings') }}</button>
      </div>

    </div>
  </div>

</div>
<script>
function display_pdf(x){
//  alert("File Name is : "+x);
  document.getElementById('hello').innerHTML = "<iframe src='/portal/public/documents/"+x+"' frameborder='0' width='100%' height='100%'></iframe>";
  //
}
</script>
@endsection
