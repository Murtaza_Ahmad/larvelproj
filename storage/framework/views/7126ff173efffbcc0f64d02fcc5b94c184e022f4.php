<?php $__env->startSection('title'); ?>
News Messages | Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Manage News Messages')); ?> </h4>
      </div>
      <?php if(session('status')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo e(session('status')); ?>

        </div>
        <?php endif; ?>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class= "text-primary">
              <th>  <?php echo e(__('Message')); ?> ID  </th>
              <th>  <?php echo e(__('News Title')); ?>  </th>
              <th>  <?php echo e(__('News Message')); ?>  </th>
              <th>  <?php echo e(__('Created At')); ?>  </th>
              <th>  <?php echo e(__('Edit')); ?>  </th>
              <th>  <?php echo e(__('Delete')); ?>  </th>
            </thead>
            <tbody>
              <!-- Displaying All Messages -->
              <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td> <?php echo e($row->id); ?> </td>
                <td style="width:25%">
                   <?php echo e($row->News_Title); ?> </td>
                <td style="width:25%;">
                  <span maxlength="50">
                  <?php echo e($row->News_Messages); ?>...
                </span></td>

                <td>
                  <?php echo e($row->created_at); ?>

                </td>
                <td>
                  <a href="<?php echo e(url('admin/NewsMessages/'.$row->id)); ?>" class="btn btn-warning"> <?php echo e(__('Edit')); ?> </a>
                </td>
                <td>
                  <a href="<?php echo e(url('admin/DeleteNewsMessages/'.$row->id)); ?>" onclick="return confirm('Are you sure you want to delete this news?')" class="btn btn-danger"> <i class="fa fa-trash"></i> <?php echo e(__('Delete')); ?> </a>
                </td>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tr>

            </tbody>
          </table>

        </div>
      </div>

    </div>
    <div class="text-center">
      <a href="<?php echo e(url('/admin/AddNewsMessage')); ?>" class="btn btn-info"><i class="fa fa-plus"></i>  <?php echo e(__('Add News Messages')); ?></a>
    </div>
  </div>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/news-messages.blade.php ENDPATH**/ ?>