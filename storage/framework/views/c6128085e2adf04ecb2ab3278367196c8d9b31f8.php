<?php $__env->startSection('title'); ?>
Users List | Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Registered Users')); ?> </h4>
      </div>
      <?php if(session('status')): ?>
        <div class="alert alert-success" role="alert">
          <?php echo e(session('status')); ?>

        </div>
        <?php endif; ?>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class= "text-primary">
              <th>
                <?php echo e(__('User')); ?> ID
              </th>
              <th>
                <?php echo e(__('Name')); ?>

              </th>
              <th>
                <?php echo e(__('Email')); ?>

              </th>
              <th>
                <?php echo e(__('Update Group Rights')); ?>

              </th>
              <th>
                <?php echo e(__('Delete User')); ?>

              </th>
            </thead>
            <tbody>

              <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($user->user_type!='admin'): ?>
              <tr>
                <td>
                <?php echo e($user->id); ?>

                </td>
                <td>
                <?php echo e($user->name); ?>

                </td>
                <td>
                <?php echo e($user->email); ?>

                </td>
                <td >
                  <button class="btn btn-info"> <?php echo e(__('Update Group Rights')); ?></button>
                </td>
                <td>
                                    <a href="<?php echo e(url('admin/DeleteUser/'.$user->id)); ?>" onclick="return confirm('Are you sure you want to delete this user?')" class="btn btn-danger"> <i class="fa fa-trash"></i> <?php echo e(__('Delete')); ?> </a>
                                  </td>
              </tr>
              <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/users-list.blade.php ENDPATH**/ ?>