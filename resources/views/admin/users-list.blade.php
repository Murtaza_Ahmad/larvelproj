@extends('layouts.master')
@section('title')
Users List | Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Registered Users') }} </h4>
      </div>
      @if(session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
        @endif
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class= "text-primary">
              <th>
                {{ __('User') }} ID
              </th>
              <th>
                {{ __('Name') }}
              </th>
              <th>
                {{ __('Email') }}
              </th>
              <th>
                {{ __('Update Group Rights') }}
              </th>
              <th>
                {{ __('Delete User') }}
              </th>
            </thead>
            <tbody>

              @foreach($users as $user)
              @if($user->user_type!='admin')
              <tr>
                <td>
                {{ $user->id }}
                </td>
                <td>
                {{ $user->name }}
                </td>
                <td>
                {{ $user->email }}
                </td>
                <td >
                  <button class="btn btn-info"> {{ __('Update Group Rights') }}</button>
                </td>
                <td>
                                    <a href="{{url('admin/DeleteUser/'.$user->id)}}" onclick="return confirm('Are you sure you want to delete this user?')" class="btn btn-danger"> <i class="fa fa-trash"></i> {{ __('Delete') }} </a>
                                  </td>
              </tr>
              @endif
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
