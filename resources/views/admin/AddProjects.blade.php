@extends('layouts.master')
@section('title')
Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Creating New Project') }} </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
  <form action="{{url('/admin/AddProjects/submit')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

  <div class="modal-body">

    <div class="form-group">
      <label for="project-title" class="col-form-label" style="color:black;">{{ __('Project Title') }}</label>
      <input type="text" name="projecttitle" class="form-control" id="projecttitle" placeholder="{{ __('Enter Project Title Here') }}"  required>
    </div>
    <div class="form-group">
      <label for="project-description" class="col-form-label" style="color:black;">{{ __('Project Description') }}</label>
      <textarea class="form-control" rows="7" placeholder="{{ __('Enter Description Here') }}" name="project_description">
      </textarea>
    </div>
    <div>
      <label for="project_file" style="color:black;">{{ __('Add File') }}:</label><br>
      <input type="file" name="project_file"  id="project-file" placeholder="Enter File Here"  required>
    </div>

  </div>

  <div class="modal-footer">
  <a href="{{ url('admin/projects') }}" type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</a>
  <button type="submit" class="btn btn-primary">{{ __('Add') }}</button>
  </div>
  </form>
</div>

</div>
</div>
</div>
</div>
@endsection
