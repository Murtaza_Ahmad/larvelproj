@extends('layouts.master')
@section('title')
News Messages | Planner
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> {{ __('Manage News Messages') }} </h4>
      </div>
      @if(session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
        @endif
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class= "text-primary">
              <th>  {{ __('Message') }} ID  </th>
              <th>  {{ __('News Title') }}  </th>
              <th>  {{ __('News Message') }}  </th>
              <th>  {{ __('Created At') }}  </th>
              <th>  {{ __('Edit') }}  </th>
              <th>  {{ __('Delete') }}  </th>
            </thead>
            <tbody>
              <!-- Displaying All Messages -->
              @foreach($messages as $row)
              <tr>
                <td> {{$row->id }} </td>
                <td style="width:25%">
                   {{$row->News_Title }} </td>
                <td style="width:25%;">
                  <span maxlength="50">
                  {{$row->News_Messages }}...
                </span></td>

                <td>
                  {{$row->created_at}}
                </td>
                <td>
                  <a href="{{url('admin/NewsMessages/'.$row->id)}}" class="btn btn-warning"> {{ __('Edit') }} </a>
                </td>
                <td>
                  <a href="{{url('admin/DeleteNewsMessages/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete this news?')" class="btn btn-danger"> <i class="fa fa-trash"></i> {{ __('Delete') }} </a>
                </td>
                @endforeach
              </tr>

            </tbody>
          </table>

        </div>
      </div>

    </div>
    <div class="text-center">
      <a href="{{url('/admin/AddNewsMessage')}}" class="btn btn-info"><i class="fa fa-plus"></i>  {{ __('Add News Messages') }}</a>
    </div>
  </div>

</div>
@endsection
