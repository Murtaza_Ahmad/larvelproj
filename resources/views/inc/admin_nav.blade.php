<ul class="nav">
  <li class="{{Request::is('admin/dashboard') ? 'active' : ''}}">
    <a href="{{ url('/admin/dashboard') }}">
      <i class="now-ui-icons design_app"></i>
      <p>{{ __('Dashboard') }}</p>
    </a>
  </li>

  <li class="{{Request::is('admin/users-list') ? 'active' : ''}}">
    <a href="{{ url('/admin/users-list') }}">
      <i class="now-ui-icons location_map-big"></i>
      <p>{{ __('Manage Registered Users') }}</p>
    </a>
  </li>

  <li class="{{Request::is('admin/projects') ? 'active' : ''}}">
    <!-- <a href="{{ url('/admin/users') }}">  -->
<a href="{{ url('/admin/projects') }}">
      <i class="now-ui-icons users_single-02"></i>
      <p>{{ __('Manage Projects') }}</p>
    </a>
  </li>
  <li class="{{Request::is('admin/ProjectAccess') ? 'active' : ''}}">
    <a href="{{ url('/admin/ProjectAccess') }}">
      <i class="now-ui-icons design_bullet-list-67"></i>
      <p>{{ __('Manage Access of Projects') }}</p>
    </a>
  </li>
  <li class="{{Request::is('admin/WeekPlanning') ? 'active' : ''}}">
    <a href="{{ url('/admin/WeekPlanning') }}">
      <i class="now-ui-icons design_bullet-list-67"></i>
      <p>{{ __('Manage Week Planning') }}</p>
    </a>
  </li>
  <li class="{{Request::is('admin/NewsMessages') ? 'active' : ''}}">
    <a href="{{ url('/admin/NewsMessages') }}">
      <i class="now-ui-icons location_map-big"></i>
      <p>{{ __('Manage News Messages') }}</p>
    </a>
  </li>
<!--
  <li>
    <a href="./typography.html">
      <i class="now-ui-icons text_caps-small"></i>
      <p>Typography</p>
    </a>
  </li>
-->
</ul>
