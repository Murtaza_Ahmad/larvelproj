<?php $__env->startSection('content'); ?>
<!-- Modal Box for testing -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="width:100%;height:100%;">
    <!--  <div class="modal-header">
-->
<div class="text-right">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span>
        </button>
</div>
  <!--    </div>
-->

      <div class="modal-body" >
        <div>
          <button class="btn btn-primary btn-sm" id="prev"><?php echo e(__('Previous')); ?></button>
          <button id="next" class="btn btn-info btn-sm"><?php echo e(__('Next')); ?></button>
          &nbsp; &nbsp;
          <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
        </div>

        <canvas id="the-canvas"></canvas>
      </div>
<!--
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
-->
    </div>
  </div>
</div>

    <section class="section">

    <!-- Code Here for the home page -->
    <br><br><br>

    <div class="col-12 col-sm-12 col-lg-12">
      <h2 style="color:black;"><?php echo e(__('News')); ?></h2>
      <div class="card card-primary">

        <br>


          <div class="card-header-action">

          </div>
        <div class="card-body">
          <?php if($messages): ?>
          <p style="text-align:right">
            <?php echo e(__('Created At')); ?> :
            <?php echo e($messages->created_at); ?>

        </p>
          <h4><?php echo e($messages->News_Title); ?></h4>
          <span>
          <p><?php echo e(str_limit($messages->News_Messages,50)); ?></p>
          </span>
          <?php else: ?>
          <p>No news to show</p>
          <?php endif; ?>
        </div>
        <div class="text-right mt-sm-0 mt-3">
            <a href="<?php echo e(url('/All-News')); ?>" class="btn btn-primary" style="font-size:15px;margin-right:12px;margin-bottom:12px;"><?php echo e(__('View All')); ?> <i class="dripicons-arrow-right"></i></a>
        </div>
      </div>

    </div>
</section>
<br><br>
<section class="section">
    <div class="col-12 col-sm-12 col-lg-12">
      <h2 style="color:black;" ><?php echo e(__('Week Planning')); ?></h2>
      <div class="card card-primary">
          <div class="card-header-action">

          </div>
        <div class="card-body">
          <h4><?php echo e(__('Latest Planning')); ?></h4>
          <div class="row text-center">
            <div class="col">
              <?php if($planning): ?>
          <h4>Week # <?php echo e($planning->Week_Number); ?></h4>
          </div>
          <div class="col">
            <button onclick="display_pdf('<?php echo e($planning->pdf_file); ?>')" data-toggle="modal" class="btn"
              data-target=".bd-example-modal-lg"><i class="fa fa-file-pdf-o" style="font-size:36px"></i></button>
          </div>
          <?php else: ?>
          <p style="text-align:left">No plannings to show</p>

          <?php endif; ?>
        </div><br>
        </div>
        <div class="text-right mt-sm-0 mt-3">
            <a href="<?php echo e(url('/All-Plannings')); ?>" class="btn btn-primary" style="font-size:15px;margin-right:12px;margin-bottom:12px;"><?php echo e(__('View All')); ?> <i class="dripicons-arrow-right"></i></a>
        </div>
      </div>

    </div>
</section>
<br>

    <section class="section">

    <div class="col-12 col-sm-12 col-lg-12">
        <h2 style="color:black;"><?php echo e(__('Projects')); ?></h2>
      <div class="card card-primary">

          <div class="card-header-action">
            <h4></h4>
          </div>
        <div class="card-body">
          <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="row text-center">
            <div class="col">
          <h4><?php echo e($proj->Project_Name); ?></h4>
          </div>
          <div class="col">
            <button data-toggle="modal" onclick="display_pdf_projects('<?php echo e($proj->pdf_file); ?>')" class="btn"
              data-target=".bd-example-modal-lg"><i class="fa fa-file-pdf-o" style="font-size:36px"></i></button>
          </div>
        </div><br>
        <br>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="text-right mt-sm-0 mt-3">
            <a href="<?php echo e(url('/All-Projects')); ?>" class="btn btn-primary" style="font-size:15px;margin-right:12px;margin-bottom:12px;"><?php echo e(__('View All')); ?> <i class="dripicons-arrow-right"></i></a>
        </div>
      </div>

    </div>
</section>
<!--
<div id="google_translate_element"></div>
-->
<script>
function display_pdf(x){
// var url = '/Folder/public/documents/test2.pdf';
  // If absolute URL from the remote server is provided, configure the CORS
  // header on that server.
 // var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf';

   var url = '/portal/public/Week_Plannings/'+x;

  // Loaded via <script> tag, create shortcut to access PDF.js exports.
  var pdfjsLib = window['pdfjs-dist/build/pdf'];

  // The workerSrc property shall be specified.
  pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

  var pdfDoc = null,
      pageNum = 1,
      pageRendering = false,
      pageNumPending = null,
      scale = 0.55,
      canvas = document.getElementById('the-canvas'),
      ctx = canvas.getContext('2d');

  /**
   * Get page info from document, resize canvas accordingly, and render page.
   * @param  num Page number.
   */
  function renderPage(num) {
    pageRendering = true;
    // Using promise to fetch the page
    pdfDoc.getPage(num).then(function(page) {
      var viewport = page.getViewport({scale: scale});
      canvas.height = viewport.height;
      canvas.width = viewport.width;

      // Render PDF page into canvas context
      var renderContext = {
        canvasContext: ctx,
        viewport: viewport
      };
      var renderTask = page.render(renderContext);

      // Wait for rendering to finish
      renderTask.promise.then(function() {
        pageRendering = false;
        if (pageNumPending !== null) {
          // New page rendering is pending
          renderPage(pageNumPending);
          pageNumPending = null;
        }
      });
    });

    // Update page counters
    document.getElementById('page_num').textContent = num;
  }

  /**
   * If another page rendering in progress, waits until the rendering is
   * finised. Otherwise, executes rendering immediately.
   */
  function queueRenderPage(num) {
    if (pageRendering) {
      pageNumPending = num;
    } else {
      renderPage(num);
    }
  }

  /**
   * Displays previous page.
   */
  function onPrevPage() {
    if (pageNum <= 1) {
      return;
    }
    pageNum--;
    queueRenderPage(pageNum);
  }
  document.getElementById('prev').addEventListener('click', onPrevPage);

  /**
   * Displays next page.
   */
  function onNextPage() {
    if (pageNum >= pdfDoc.numPages) {
      return;
    }
    pageNum++;
    queueRenderPage(pageNum);
  }
  document.getElementById('next').addEventListener('click', onNextPage);

  /**
   * Asynchronously downloads PDF.
   */
  pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
    pdfDoc = pdfDoc_;
    document.getElementById('page_count').textContent = pdfDoc.numPages;

    // Initial/first page rendering
    renderPage(pageNum);
  });

}

function display_pdf_projects(x){
  // var url = '/Folder/public/documents/test2.pdf';
    // If absolute URL from the remote server is provided, configure the CORS
    // header on that server.
   // var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/web/compressed.tracemonkey-pldi-09.pdf';

     var url = '/portal/public/documents/'+x;

    // Loaded via <script> tag, create shortcut to access PDF.js exports.
    var pdfjsLib = window['pdfjs-dist/build/pdf'];

    // The workerSrc property shall be specified.
    pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

    var pdfDoc = null,
        pageNum = 1,
        pageRendering = false,
        pageNumPending = null,
        scale = 0.55,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d');

    /**
     * Get page info from document, resize canvas accordingly, and render page.
     * @param  num Page number.
     */
    function renderPage(num) {
      pageRendering = true;
      // Using promise to fetch the page
      pdfDoc.getPage(num).then(function(page) {
        var viewport = page.getViewport({scale: scale});
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = {
          canvasContext: ctx,
          viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(function() {
          pageRendering = false;
          if (pageNumPending !== null) {
            // New page rendering is pending
            renderPage(pageNumPending);
            pageNumPending = null;
          }
        });
      });

      // Update page counters
      document.getElementById('page_num').textContent = num;
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    function queueRenderPage(num) {
      if (pageRendering) {
        pageNumPending = num;
      } else {
        renderPage(num);
      }
    }

    /**
     * Displays previous page.
     */
    function onPrevPage() {
      if (pageNum <= 1) {
        return;
      }
      pageNum--;
      queueRenderPage(pageNum);
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    /**
     * Displays next page.
     */
    function onNextPage() {
      if (pageNum >= pdfDoc.numPages) {
        return;
      }
      pageNum++;
      queueRenderPage(pageNum);
    }
    document.getElementById('next').addEventListener('click', onNextPage);

    /**
     * Asynchronously downloads PDF.
     */
    pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
      pdfDoc = pdfDoc_;
      document.getElementById('page_count').textContent = pdfDoc.numPages;

      // Initial/first page rendering
      renderPage(pageNum);
    });

  }

  //document.getElementById('hello').innerHTML = "<embed src='/portal/public/documents/"+x+"' frameborder='0' width='100%' height='100%'>";
  //document.getElementById('hello').innerHTML = document.getElementById('the-canvas').innerHTML;
  //}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/home.blade.php ENDPATH**/ ?>