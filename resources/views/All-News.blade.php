@extends('layouts.main')

@section('content')

    <section class="section">

    <!-- Code Here for the home page -->
    <br><br><br>

    <div class="col-12 col-sm-12 col-lg-12">
      <h2 style="color:black;">{{ __('News') }}</h2>
      @foreach($news as $message)
      <div class="card card-primary">


          <div class="card-header-action">

          </div>
        <div class="card-body">
          <p style="text-align:right">
            {{ __('Created At') }} :
            {{ $message->created_at }}
        </p>
        <h4>{{ $message->News_Title }}</h4>
          <p>{{ $message->News_Messages }}</p>
        </div>
        <div class="text-right mt-sm-0 mt-3">
            <a href="{{url('/All-News/'.$message->id)}}" class="btn btn-primary" style="font-size:15px;margin-right:12px;margin-bottom:12px;">{{ __('Read Further') }} :<i class="dripicons-arrow-right"></i></a>
        </div>
      </div><br>
@endforeach
    </div>
</section>
<br>

@endsection
