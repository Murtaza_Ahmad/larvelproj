<ul class="nav">
  <li class="<?php echo e(Request::is('admin/dashboard') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/admin/dashboard')); ?>">
      <i class="now-ui-icons design_app"></i>
      <p><?php echo e(__('Dashboard')); ?></p>
    </a>
  </li>

  <li class="<?php echo e(Request::is('admin/users-list') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/admin/users-list')); ?>">
      <i class="now-ui-icons location_map-big"></i>
      <p><?php echo e(__('Manage Registered Users')); ?></p>
    </a>
  </li>

  <li class="<?php echo e(Request::is('admin/projects') ? 'active' : ''); ?>">
    <!-- <a href="<?php echo e(url('/admin/users')); ?>">  -->
<a href="<?php echo e(url('/admin/projects')); ?>">
      <i class="now-ui-icons users_single-02"></i>
      <p><?php echo e(__('Manage Projects')); ?></p>
    </a>
  </li>
  <li class="<?php echo e(Request::is('admin/ProjectAccess') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/admin/ProjectAccess')); ?>">
      <i class="now-ui-icons design_bullet-list-67"></i>
      <p><?php echo e(__('Manage Access of Projects')); ?></p>
    </a>
  </li>
  <li class="<?php echo e(Request::is('admin/WeekPlanning') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/admin/WeekPlanning')); ?>">
      <i class="now-ui-icons design_bullet-list-67"></i>
      <p><?php echo e(__('Manage Week Planning')); ?></p>
    </a>
  </li>
  <li class="<?php echo e(Request::is('admin/NewsMessages') ? 'active' : ''); ?>">
    <a href="<?php echo e(url('/admin/NewsMessages')); ?>">
      <i class="now-ui-icons location_map-big"></i>
      <p><?php echo e(__('Manage News Messages')); ?></p>
    </a>
  </li>
<!--
  <li>
    <a href="./typography.html">
      <i class="now-ui-icons text_caps-small"></i>
      <p>Typography</p>
    </a>
  </li>
-->
</ul>
<?php /**PATH D:\Xampp\htdocs\Folder\resources\views/inc/admin_nav.blade.php ENDPATH**/ ?>