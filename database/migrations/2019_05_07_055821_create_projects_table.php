<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Project_Title');
            //$table->enum('status',['Ongoing','Finished','OnHold'])->default('Ongoing');
            $table->text('Description');
            $table->mediumText('pdf_file')->nullable();
            //$table->date('start_date')->nullable();
            //$table->date('end_date')->nullable();
            //$table->integer('budget')->default(0);
            //$table->integer('user_id')->nullable();    //User_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
