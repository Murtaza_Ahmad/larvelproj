<?php $__env->startSection('title'); ?>
Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Creating News Message')); ?> </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
  <form action="<?php echo e(url('/admin/AddNewsMessage/submit')); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>


  <div class="modal-body">

    <div class="form-group">
      <label for="news-title" class="col-form-label" style="color:black;"><?php echo e(__('News Title')); ?>:</label>
      <input type="text" name="newstitle" class="form-control" id="newstitle" placeholder="<?php echo e(__('Enter News Title Here')); ?>"  required>
    </div>
    <div class="form-group">
      <label for="news-description" class="col-form-label" style="color:black;"><?php echo e(__('News Message')); ?></label>
      <textarea class="form-control" rows="15" maxlength="1000" placeholder="<?php echo e(__('Enter News Message Here')); ?>" name="news_message" required>
      </textarea>
    </div>
  <!--  <div class="form-group">
      <label for="text-message" class="col-form-label" style="color:black;"><?php echo e(__('Text Message')); ?></label>
      <textarea class="form-control" rows="7" placeholder="<?php echo e(__('Enter Text Message Here')); ?>" name="text_message">
      </textarea>
    </div>
  -->
  </div>

  <div class="modal-footer">
  <a href="<?php echo e(url('admin/NewsMessages')); ?>" type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></a>
  <button type="submit" class="btn btn-primary"><?php echo e(__('Add')); ?></button>
  </div>
  </form>
</div>

</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/AddNewsMessages.blade.php ENDPATH**/ ?>