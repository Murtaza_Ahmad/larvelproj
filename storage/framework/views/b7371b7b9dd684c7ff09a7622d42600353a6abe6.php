<?php $__env->startSection('title'); ?>
Planner
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title"> <?php echo e(__('Edit Week Planning')); ?> </h4>
      </div>
      <div class="card-body">
<div class="jumbotron">
<?php $__currentLoopData = $planning; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <form action="<?php echo e(url('admin/UpdateWeekPlanning/'.$plan->id)); ?>" method="post" enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

    <?php echo e(method_field('PUT')); ?>

  <div class="modal-body">

    <div class="form-group">
      <label for="planning-title" class="col-form-label"><?php echo e(__('Week Planning Title')); ?>:</label>
      <input type="text" name="planning_title" class="form-control" id="planning_title" placeholder="<?php echo e(__('Enter Planning Title')); ?>" required value="<?php echo e($plan->Title); ?>">
    </div>
    <div class="form-group">
      <label for="recipient-name" class="col-form-label"><?php echo e(__('Enter Week #')); ?>:</label>
      <select name="planning_week" id="planning_week" class="form-control" required>
        <option selected value="<?php echo e($plan->Week_Number); ?>"><?php echo e($plan->Week_Number); ?></option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
      </select>
    </div>

    <div>
      <label for="project_file" style="color:black;"><?php echo e(__('Add File')); ?> :</label><br>
      <input type="file" name="project_file"  id="project-file" placeholder="Enter File Here"  required value="<?php echo e($plan->pdf_file); ?>">
    </div>

  </div>

  <div class="modal-footer">
  <a href="<?php echo e(url('admin/projects')); ?>" type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></a>
  <button type="submit" class="btn btn-primary"><?php echo e(__('Update')); ?></button>
  </div>
  </form>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Xampp\htdocs\Folder\resources\views/admin/EditWeekPlanning.blade.php ENDPATH**/ ?>