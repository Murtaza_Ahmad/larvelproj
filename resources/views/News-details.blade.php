@extends('layouts.main')

@section('content')

    <section class="section">

    <!-- Code Here for the home page -->
    <br>

    <div class="col-12 col-sm-12 col-lg-8">
      @foreach($data as $message)
      <div class="card card-primary">

        <div class="card-body">
          <p style="text-align:right">
            {{ __('Created At') }} :  {{ $message->created_at }}
          </p>
        <h4 style="color:black;">{{ $message->News_Title }}</h4>
          <p>{{ $message->News_Messages }}</p>
        </div>
      </div><br>
@endforeach
    </div>
</section>
<br>

@endsection
